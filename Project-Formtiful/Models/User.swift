struct User {
    var firstName: String
    var email: String?
    var password: String
    var website: String?
    
    init(
        firstName: String,
        email: String? = nil,
        password: String,
        website: String? = nil
    ) {
        self.firstName = firstName
        self.email = email
        self.password = password
        self.website = website
    }
}
