//
//  Project_FormtifulApp.swift
//  Project-Formtiful
//
//  Created by d0yc on 10/25/21.
//

import SwiftUI

@main
struct Project_FormtifulApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
