import SwiftUI

struct ContentView: View {
    @State var username: String = ""
    @State var email: String = ""
    @State var password: String = ""
    @State var website: String = ""
    @State var isActive = false
    
    init() {
        UITableView.appearance().backgroundColor = .clear
    }
    
    var body: some View {
        NavigationView {
            VStack {
                NavigationLink(
                    destination: ConfirmationView(
                        user: User(
                            firstName: username,
                            email: email,
                            password: password,
                            website: website
                        )
                    ),
                    isActive: $isActive
                ) {}
                
                Text("Use the form below to submit your portfolio. An email and password are required.")
                Form {
                    Group {
                        TextField("First Name", text: $username)
                        TextField("Email Address", text: $email)
                        SecureField("Password", text: $password)
                        TextField("Website", text: $website)
                    }
                    .textFieldStyle(.roundedBorder)
                    .overlay(RoundedRectangle(cornerRadius: 5).stroke())
                }
                
                Button("Submit", action: {
                    isActive = true
                }).disabled(isDisabled)
                    .font(Font.custom("Helvetica Neue", size: 20))
                    .padding(10)
                    .frame(maxWidth: .infinity)
                    .foregroundColor(Color.white)
                    .background(
                        isDisabled ? gradientGrayButton : gradientRedButton
                    )
                    .cornerRadius(10)
            }
            .navigationTitle("Profile Creation")
            .padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
        }
    }
    
    // Check if username or password is not empty
    var isDisabled: Bool {
        if !username.isEmpty && !password.isEmpty {
            return false
        }
        return true
    }
}

// Reusable gradient buttons
var gradientRedButton: LinearGradient {
    return LinearGradient(gradient: Gradient(colors: [.red, .orange]), startPoint: .leading, endPoint: .trailing)
}

var gradientGrayButton: LinearGradient {
    return LinearGradient(gradient: Gradient(colors: [.gray, .black]), startPoint: .leading, endPoint: .trailing)
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
