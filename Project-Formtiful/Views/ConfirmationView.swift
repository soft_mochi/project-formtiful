import SwiftUI

struct ConfirmationView: View {
    var user: User
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                HStack {
                    Text("Hello \(user.firstName)!")
                        .bold()
                        .font(.largeTitle)
                        .frame(alignment: .leading)
                        .padding(5)
                    Image(systemName: "face.smiling")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 40, height: 40)
                }
                
                VStack(alignment: .center) {
                    Text("Your super-awesome portfolio has been successfully submitted! The details below will be public within your community!")
                    
                    if let website = user.website, !website.isEmpty {
                        Link(destination: URL(string: website)!, label: {
                            Text(website).underline()
                        })
                    }
                    
                    Text(user.firstName)
                    
                    if let email = user.email, !email.isEmpty {
                        Text(email)
                    }
                    
                    Spacer()
                    
                    Button("Sign in", action: {})
                        .font(Font.custom("Helvetica Neue", size: 20))
                        .frame(maxWidth: .infinity)
                        .padding(10)
                        .foregroundColor(Color.white)
                        .background(gradientRedButton)
                        .cornerRadius(10)
                }
            }
            .navigationBarHidden(true)
            .padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 15))
        }
    }
}

struct ConfirmationView_Preview: PreviewProvider {
    static var previews: some View {
        ConfirmationView(
            user: User(
                firstName: "Jacki",
                email: "jacki@gmail.com",
                password: "SecretPassword",
                website: "jacki.com"
            )
        )
    }
}
